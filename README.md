# parcel_perform_interview_ops

### Author: Dat Ton
### Email: datton94@gmail.com

This is the project for the interview with Parcel Perform. 

Any ideas to improve the source code are welcome :)

### Introduction

This is the Ops repository where store terraform source code and credential secret related to infrastructure. 

Credential was encrypted by SOPS from Mozilla. We can use git-secret or git-crypt, all they are good choice. 

Terraform will have many modules, as you can see in the `terraform/modules` 

And in the dev directory, I create many component, each infra component I create a directory, because I want to separate the `.tfstate` file to reduce the risk to get damage state file as low as possible. If we combine all resource to one state file, it's ok also, but if we did a change, it could hurt the state file and all other resources which is in the same state file.

I have setup a CI/CD flow for this repo 

Basically, when we push a new commit:

New commit --> CI (shell check, terraform format check, terraform plan) ----> CD ( terraform apply)
