#!/usr/bin/env bash

set +x
set -e

cd "$(git rev-parse --show-toplevel)" || exit 

while read -r line;
    do 
        (
            shellcheck "$line" 1> result.txt || true
            if grep -q '[^[:space:]]' "result.txt"; then
                cat result.txt
                echo "You need to fix your bash shell"
                exit 1
            fi
        )
done <<< "$(find . -name '*.sh' -type f)"
