#!/usr/bin/env bash

set +x
set +e

TF_VERSION="${TF_VERSION:-0.14.4}"
#cd "$(git rev-parse --show-toplevel)" || exit 

if ! (command terraform -v > /dev/null); then
    wget -c "https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_amd64.zip" || exit 1
fi

unzip -qq "terraform_${TF_VERSION}_linux_amd64.zip" -d tfbin
PATH="$(pwd -P)"/tfbin:$PATH
export PATH

terraform fmt -recursive || exit 1
