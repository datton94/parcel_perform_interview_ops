#!/usr/bin/env bash
set +e
set +x

TF_VERSION="${TF_VERSION:-0.14.4}"

if ! (command terraform -v > /dev/null); then
    wget -c "https://releases.hashicorp.com/terraform/${TF_VERSION}/terraform_${TF_VERSION}_linux_amd64.zip" || exit 1
fi

unzip -qq "terraform_${TF_VERSION}_linux_amd64.zip" -d tfbin
PATH="$(pwd -P)"/tfbin:$PATH
export PATH

while read -r line;
    do 
      (
          echo "planning $line"
          cd "$line" && terraform init && terraform plan
      )
done <<< "$(find . -type d | grep -v modules | grep -v bootstrap)"
