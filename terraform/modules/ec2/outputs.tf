output "public_dns" {
  value = aws_instance.default[0].public_dns
}

output "public_ip" {
  value = aws_instance.default[0].public_ip
}

output "private_dns" {
  value = aws_instance.default[0].private_dns
}

output "arn" {
  value = aws_instance.default[0].arn
}