resource "aws_internet_gateway" "igw" {
  vpc_id = aws_vpc.vpc.id

  tags = var.tags
}

resource "aws_route_table" "prod-public-crt" {
  vpc_id = aws_vpc.vpc.id

  route {
    //machine in this subnet can reach everywhere
    cidr_block = "0.0.0.0/0"
    //CRT uses this IGW to reach internet
    gateway_id = aws_internet_gateway.igw.id
  }

  tags = merge(
    var.tags,
    {
      Name = "${var.name}-public-crt"
    }
  )
}

resource "aws_route_table" "prod-private-crt" {
  vpc_id = aws_vpc.vpc.id

  tags = merge(
    var.tags,
    {
      Name = "${var.name}-private-crt"
    }
  )
}

resource "aws_route_table_association" "prod-crta-public" {
  for_each       = var.public-subnet-numbers
  subnet_id      = aws_subnet.public[each.key].id
  route_table_id = aws_route_table.prod-public-crt.id
}

resource "aws_route_table_association" "prod-crta-private" {
  for_each       = var.private-subnet-numbers
  subnet_id      = aws_subnet.private[each.key].id
  route_table_id = aws_route_table.prod-private-crt.id
}
