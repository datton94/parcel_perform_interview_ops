locals {
  cluster_name    = "eks-dev"
  cluster_version = "1.18"
  account_id      = "438723512299"

  main_private_subnets = [
    data.terraform_remote_state.network.outputs.private-subnet-0.id,
    data.terraform_remote_state.network.outputs.private-subnet-1.id,
    data.terraform_remote_state.network.outputs.private-subnet-2.id,
  ]

  main_public_subnets = [
    data.terraform_remote_state.network.outputs.public-subnet-0.id,
    data.terraform_remote_state.network.outputs.public-subnet-1.id,
    data.terraform_remote_state.network.outputs.public-subnet-2.id,
  ]

  worker_groups = [
    {
      // Default workers handle some critical ops tasks.
      // We keep them small at this step. Autoscaling feature will help
      // to reduce the size of the cluster.
      name          = "default"
      key_name      = module.eks_ssh_key_pair.key_name
      instance_type = "t3.small"
      // This argument below was set because to prevent new default ami update through time used in the module eks (filter most recent = true)
      // If you want to update the ami to the newest with support for EBS optimize, please remove 'ami_id'
      // ami_id = "ami-0b9d2c11b47bd8264"
      //
      autoscaling_enabled   = true
      protect_from_scale_in = true
      asg_desired_capacity  = "1"
      asg_max_size          = "15"
      asg_min_size          = "1"
      root_volume_size      = "20"
      public_ip             = true
      subnets               = [join(",", local.main_public_subnets)]
      // NOTE: This is required for ALB setup
      worker_additional_security_group_ids = aws_security_group.aws-alb-ingress-sg-healthceck.id
      kubelet_extra_args                   = "--node-labels=mine/group=default,mine/role=default"
      tags = [
        {
          key                 = "k8s.io/role/node"
          value               = local.cluster_name
          propagate_at_launch = "1"
        },
        {
          key                 = "k8s.io/cluster-autoscaler/enabled"
          value               = "1"
          propagate_at_launch = "1"
        },
        {
          key                 = "k8s.io/cluster-autoscaler/${local.cluster_name}"
          value               = "1"
          propagate_at_launch = "1"
        },
      ]
    },
    {
      name          = "staging"
      key_name      = module.eks_ssh_key_pair.key_name
      instance_type = "t3.small"
      // This argument below was set because to prevent new default ami update through time used in the module eks (filter most recent = true)
      // If you want to update the ami to the newest with support for EBS optimize, please remove 'ami_id'
      // ami_id = "ami-0b9d2c11b47bd8264"
      //
      autoscaling_enabled   = true
      protect_from_scale_in = true
      asg_desired_capacity  = "0"
      asg_max_size          = "10"
      asg_min_size          = "0"
      public_ip             = true
      subnets               = [join(",", local.main_public_subnets)]
      // NOTE: This is required for ALB setup
      worker_additional_security_group_ids = aws_security_group.aws-alb-ingress-sg-healthceck.id
      kubelet_extra_args                   = "--node-labels=mine/group=staging,mine/role=default"
      tags = [
        {
          key                 = "k8s.io/cluster-autoscaler/enabled"
          value               = "1"
          propagate_at_launch = "1"
        },
        {
          key                 = "k8s.io/cluster-autoscaler/${local.cluster_name}"
          value               = "1"
          propagate_at_launch = "1"
        },
        {
          key                 = "k8s.io/cluster-autoscaler/node-template/label/mine/group"
          value               = "staging"
          propagate_at_launch = "1"
        },

      ]
    },
    {
      name          = "production"
      key_name      = module.eks_ssh_key_pair.key_name
      instance_type = "t3.small"
      // This argument below was set because to prevent new default ami update through time used in the module eks (filter most recent = true)
      // If you want to update the ami to the newest with support for EBS optimize, please remove 'ami_id'
      // ami_id = "ami-0b9d2c11b47bd8264"
      //
      autoscaling_enabled   = true
      protect_from_scale_in = true
      asg_desired_capacity  = "0"
      asg_max_size          = "40"
      asg_min_size          = "0"
      public_ip             = true
      subnets               = [join(",", local.main_public_subnets)]
      // NOTE: This is required for ALB setup
      worker_additional_security_group_ids = aws_security_group.aws-alb-ingress-sg-healthceck.id
      kubelet_extra_args                   = "--node-labels=mine/group=production,mine/role=default"
      tags = [
        {
          key                 = "k8s.io/cluster-autoscaler/enabled"
          value               = "1"
          propagate_at_launch = "1"
        },
        {
          key                 = "k8s.io/cluster-autoscaler/${local.cluster_name}"
          value               = "1"
          propagate_at_launch = "1"
        },
        {
          key                 = "k8s.io/cluster-autoscaler/node-template/label/mine/group"
          value               = "production"
          propagate_at_launch = "1"
        },
      ]
    },
    {
      name          = "logging"
      key_name      = module.eks_ssh_key_pair.key_name
      instance_type = "t3.small"
      // This argument below was set because to prevent new default ami update through time used in the module eks (filter most recent = true)
      // If you want to update the ami to the newest with support for EBS optimize, please remove 'ami_id'
      // ami_id = "ami-0b9d2c11b47bd8264"
      //
      autoscaling_enabled   = true
      protect_from_scale_in = true
      asg_desired_capacity  = "0"
      asg_max_size          = "10"
      asg_min_size          = "0"
      public_ip             = true
      subnets               = [join(",", local.main_public_subnets)]
      // NOTE: This is required for ALB setup
      worker_additional_security_group_ids = aws_security_group.aws-alb-ingress-sg-healthceck.id
      kubelet_extra_args                   = "--node-labels=mine/group=logging,mine/role=default"
      tags = [
        {
          key                 = "k8s.io/cluster-autoscaler/enabled"
          value               = "1"
          propagate_at_launch = "1"
        },
        {
          key                 = "k8s.io/cluster-autoscaler/${local.cluster_name}"
          value               = "1"
          propagate_at_launch = "1"
        },
        {
          key                 = "k8s.io/cluster-autoscaler/node-template/label/mine/group"
          value               = "logging"
          propagate_at_launch = "1"
        },
      ]
    }
  ]

  map_roles = [
    {
      role_arn = "arn:aws:iam::438723512299:role/AdminAccess"
      username = "admin"
      group    = "system:masters"
    },
  ]
}

module "eks" {
  source  = "terraform-aws-modules/eks/aws"
  version = "6.0.0"

  #map_roles = local.map_roles

  cluster_enabled_log_types     = ["api", "audit", "authenticator", "controllerManager", "scheduler"]
  cluster_log_retention_in_days = 1
  cluster_name                  = local.cluster_name
  cluster_version               = local.cluster_version

  worker_groups = local.worker_groups

  # WARNING: This is just enough. Please use kube2iam instead.
  # WARNING: Don't add any futher policies

  workers_additional_policies = [
    aws_iam_policy.eks_worker_policies-eks-dev.arn,
  ]

  kubeconfig_aws_authenticator_env_variables = {
    AWS_PROFILE = "victor"
  }

  # The existing VPC and subnets had to be tagged as per requirements
  # of EKS: https://docs.aws.amazon.com/eks/latest/userguide/network_reqs.html
  vpc_id = data.terraform_remote_state.network.outputs.parcel-perform-interview-vpc.id

  subnets = local.main_public_subnets
}

resource "aws_iam_policy" "eks_worker_policies-eks-dev" {
  description = "${local.cluster_name} worker policies"
  name        = "${local.cluster_name}-worker-policies"

  policy = <<EOS
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        "sts:AssumeRole"
      ],
      "Resource": [
        "arn:aws:iam::${local.account_id}:role/k8s-kube2iam-*"
      ]
    }
  ]
}
EOS

}



module "eks_ssh_key_pair" {
  source                = "../../modules/key_pair"
  environment           = "dev"
  project               = "Parcel-perform-interview"
  name                  = "eks-dev-ssh-key-pair"
  ssh_public_key_path   = "./"
  private_key_extension = ".pem"
  public_key_extension  = ".pub"
  generate_ssh_key      = true
}
