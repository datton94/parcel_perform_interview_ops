# This file was part of the main.tf. It would be part of a complete
# ALB/autoscaling setup.

resource "aws_security_group" "aws-alb-ingress-sg" {
  name        = "aws-alb-ingress-sg"
  description = "Security Group for the AWS ALB Zalando ingress controller"
  vpc_id      = data.terraform_remote_state.network.outputs.parcel-perform-interview-vpc.id

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  egress {
    to_port     = 80
    from_port   = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 30080
    to_port     = 30080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "kubernetes.io/cluster/eks-euw1" = "owned"
    "kubernetes:application"         = "kube-ingress-aws-controller"
    terraformed                      = "true"
  }
}

resource "aws_security_group" "aws-alb-ingress-sg-healthceck" {
  name        = "aws-alb-ingress-sg-healthceck"
  description = "Security Group for the AWS ALB Zalando health check"
  vpc_id      = data.terraform_remote_state.network.outputs.parcel-perform-interview-vpc.id

  ingress {
    from_port       = 30080
    to_port         = 30080
    protocol        = "tcp"
    security_groups = [aws_security_group.aws-alb-ingress-sg.id]
  }

  egress {
    from_port       = 30080
    to_port         = 30080
    protocol        = "tcp"
    security_groups = [aws_security_group.aws-alb-ingress-sg.id]
  }
}
