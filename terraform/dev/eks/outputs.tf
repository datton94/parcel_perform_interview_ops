output "default_workers_user_data" {
  description = "User data of default worker group"
  value       = module.eks.workers_user_data[0]
}

output "staging_workers_user_data" {
  description = "User data of staging worker group"
  value       = module.eks.workers_user_data[1]
}

output "production_workers_user_data" {
  description = "User data of production worker group"
  value       = module.eks.workers_user_data[2]
}

output "logging_workers_user_data" {
  description = "User data of logging worker group"
  value       = module.eks.workers_user_data[3]
}

output "default_iam_instance_profile" {
  description = "Iam instance profile name of default worker group"
  value       = module.eks.worker_iam_instance_profile_names[0]
}

output "staging_iam_instance_profile" {
  description = "Iam instance profile name of staging worker group"
  value       = module.eks.worker_iam_instance_profile_names[1]
}


output "production_iam_instance_profile" {
  description = "Iam instance profile name of production of worker group"
  value       = module.eks.worker_iam_instance_profile_names[2]
}

output "logging_iam_instance_profile" {
  description = "Iam instance profile name of logging worker group"
  value       = module.eks.worker_iam_instance_profile_names[3]
}
