resource "aws_security_group" "ingress-nginx-internal" {
  name        = "ingress-nginx-internal"
  description = "Security Group for the AWS ALB Zalando ingress controller"
  vpc_id      = data.terraform_remote_state.network.outputs.parcel-perform-interview-vpc.id

  ingress {
    from_port = 80
    to_port   = 80
    protocol  = "tcp"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  ingress {
    from_port = 443
    to_port   = 443
    protocol  = "tcp"

    cidr_blocks = [
      "0.0.0.0/0",
    ]
  }

  egress {
    to_port     = 80
    from_port   = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 443
    to_port     = 443
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 30081
    to_port     = 30081
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    "kubernetes.io/cluster/eks-euw1" = "owned"
    "kubernetes:application"         = "nginx-internal"
    terraformed                      = "true"
  }
}

resource "aws_security_group" "ingress-nginx-internal-healthceck" {
  name        = "ingress-nginx-internal-healthceck"
  description = "Security Group for the AWS ALB Zalando health check"
  vpc_id      = data.terraform_remote_state.network.outputs.parcel-perform-interview-vpc.id

  ingress {
    from_port       = 30081
    to_port         = 30081
    protocol        = "tcp"
    security_groups = [aws_security_group.ingress-nginx-internal.id]
  }

  egress {
    from_port       = 30081
    to_port         = 30081
    protocol        = "tcp"
    security_groups = [aws_security_group.ingress-nginx-internal.id]
  }
}

