module "ecr" {
  source      = "../../modules/ecr"
  name        = "parcel_perform_interview"
  project     = "parcel_perform_interview"
  environment = "dev"
  owner       = "Victor Vu"
}
