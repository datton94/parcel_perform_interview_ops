provider "aws" {
  region  = "ap-southeast-1"
  profile = "victor"
}

terraform {
  backend "s3" {
    bucket         = "parcel-perform-interview"
    key            = "ecr/terraform.tfstate"
    region         = "ap-southeast-1"
    dynamodb_table = "parcel_perform_interview"
    profile        = "victor"
    encrypt        = true
    kms_key_id     = "eb60ea95-446a-4191-ba0d-e37012ef30ce"
  }
} 