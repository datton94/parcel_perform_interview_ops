module "parcel-perform-network-label" {
  source = "../../modules/tags"

  name        = "vpc"
  project     = "parcel-perform-interview"
  environment = "dev"
  owner       = "Victor Vu"

  tags = {
    Description = "managed by Terraform",
  }
}

module "parcel-perform-network" {
  source = "../../modules/network"

  vpc-cidr-block = var.vpc-cidr-block

  vpc-endpoint-s3-enable = false
  nat-gw-enable          = true

  public-subnet-numbers  = var.public-subnet-numbers
  private-subnet-numbers = var.private-subnet-numbers

  name        = var.name
  project     = var.project-name
  environment = var.environment
  owner       = var.owner

  tags = module.parcel-perform-network-label.tags
}
