output "parcel-perform-interview-vpc" {
  value = module.parcel-perform-network.vpc
}

output "db-subnet-group" {
  value = module.parcel-perform-network.db-subnet-group
}

output "public-subnet-0" {
  value = module.parcel-perform-network.public-subnet-0
}

output "public-subnet-1" {
  value = module.parcel-perform-network.public-subnet-1
}

output "public-subnet-2" {
  value = module.parcel-perform-network.public-subnet-2
}

output "private-subnet-0" {
  value = module.parcel-perform-network.private-subnet-0
}

output "private-subnet-1" {
  value = module.parcel-perform-network.private-subnet-1
}

output "private-subnet-2" {
  value = module.parcel-perform-network.private-subnet-2
}

output "route-table" {
  value = module.parcel-perform-network.route-table
}
