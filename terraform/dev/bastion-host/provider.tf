provider "aws" {
  region  = "ap-southeast-1"
  profile = "victor"
}

terraform {
  backend "s3" {
    bucket         = "parcel-perform-interview"
    key            = "bastion-host/terraform.tfstate"
    region         = "ap-southeast-1"
    dynamodb_table = "parcel_perform_interview"
    profile        = "victor"
    encrypt        = true
    kms_key_id     = "eb60ea95-446a-4191-ba0d-e37012ef30ce"
  }
}

data "terraform_remote_state" "network" {
  backend = "s3"
  config = {
    bucket  = "parcel-perform-interview"
    region  = "ap-southeast-1"
    key     = "network-infra/terraform.tfstate"
    profile = "victor"
  }
}

terraform {
  required_version = "~> 0.14.4"
}