security-group-name = "bastion-host"

project = "parcel-perform-interview"

environment = "dev"

owner = "Victor Vu"

iam-role-default-name = "bastion-host"

iam-instance-profile-name = "bastion-host"

